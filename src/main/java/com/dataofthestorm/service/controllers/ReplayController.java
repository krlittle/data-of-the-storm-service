package com.dataofthestorm.service.controllers;

import com.dataofthestorm.service.models.response.ReplayResponse;
import com.dataofthestorm.service.services.ReplayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReplayController {

    @Autowired
    private ReplayService replayService;

    @GetMapping("/replays")
    public ResponseEntity<ReplayResponse> getReplay() {

        return ResponseEntity.ok(replayService.getReplaysByPlayer());
    }
}
