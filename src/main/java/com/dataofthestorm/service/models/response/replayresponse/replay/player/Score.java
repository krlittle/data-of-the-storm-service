package com.dataofthestorm.service.models.response.replayresponse.replay.player;

public class Score {

    private int level;
    private int kills;
    private int assists;
    private int takedowns;
    private int deaths;
    private int highest_kill_streak;
    private int hero_damage;
    private int siege_damage;
    private int structure_damage;
    private int minion_damage;
    private int creep_damage;
    private int summon_damage;
    private int time_cc_enemy_heroes;
    private Integer healing;
    private int self_healing;
    private Integer damage_taken;
    private int experience_contribution;
    private int town_kills;
    private int time_spent_dead;
    private int merc_camp_captures;
    private int watch_tower_captures;
    private int meta_experience;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getKills() {
        return kills;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }

    public int getAssists() {
        return assists;
    }

    public void setAssists(int assists) {
        this.assists = assists;
    }

    public int getTakedowns() {
        return takedowns;
    }

    public void setTakedowns(int takedowns) {
        this.takedowns = takedowns;
    }

    public int getDeaths() {
        return deaths;
    }

    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }

    public int getHighest_kill_streak() {
        return highest_kill_streak;
    }

    public void setHighest_kill_streak(int highest_kill_streak) {
        this.highest_kill_streak = highest_kill_streak;
    }

    public int getHero_damage() {
        return hero_damage;
    }

    public void setHero_damage(int hero_damage) {
        this.hero_damage = hero_damage;
    }

    public int getSiege_damage() {
        return siege_damage;
    }

    public void setSiege_damage(int siege_damage) {
        this.siege_damage = siege_damage;
    }

    public int getStructure_damage() {
        return structure_damage;
    }

    public void setStructure_damage(int structure_damage) {
        this.structure_damage = structure_damage;
    }

    public int getMinion_damage() {
        return minion_damage;
    }

    public void setMinion_damage(int minion_damage) {
        this.minion_damage = minion_damage;
    }

    public int getCreep_damage() {
        return creep_damage;
    }

    public void setCreep_damage(int creep_damage) {
        this.creep_damage = creep_damage;
    }

    public int getSummon_damage() {
        return summon_damage;
    }

    public void setSummon_damage(int summon_damage) {
        this.summon_damage = summon_damage;
    }

    public int getTime_cc_enemy_heroes() {
        return time_cc_enemy_heroes;
    }

    public void setTime_cc_enemy_heroes(int time_cc_enemy_heroes) {
        this.time_cc_enemy_heroes = time_cc_enemy_heroes;
    }

    public Integer getHealing() {
        return healing;
    }

    public void setHealing(Integer healing) {
        this.healing = healing;
    }

    public int getSelf_healing() {
        return self_healing;
    }

    public void setSelf_healing(int self_healing) {
        this.self_healing = self_healing;
    }

    public Integer getDamage_taken() {
        return damage_taken;
    }

    public void setDamage_taken(Integer damage_taken) {
        this.damage_taken = damage_taken;
    }

    public int getExperience_contribution() {
        return experience_contribution;
    }

    public void setExperience_contribution(int experience_contribution) {
        this.experience_contribution = experience_contribution;
    }

    public int getTown_kills() {
        return town_kills;
    }

    public void setTown_kills(int town_kills) {
        this.town_kills = town_kills;
    }

    public int getTime_spent_dead() {
        return time_spent_dead;
    }

    public void setTime_spent_dead(int time_spent_dead) {
        this.time_spent_dead = time_spent_dead;
    }

    public int getMerc_camp_captures() {
        return merc_camp_captures;
    }

    public void setMerc_camp_captures(int merc_camp_captures) {
        this.merc_camp_captures = merc_camp_captures;
    }

    public int getWatch_tower_captures() {
        return watch_tower_captures;
    }

    public void setWatch_tower_captures(int watch_tower_captures) {
        this.watch_tower_captures = watch_tower_captures;
    }

    public int getMeta_experience() {
        return meta_experience;
    }

    public void setMeta_experience(int meta_experience) {
        this.meta_experience = meta_experience;
    }
}
