package com.dataofthestorm.service.models.response;

import com.dataofthestorm.service.models.response.replayresponse.Replay;

public class ReplayResponse {

    private Replay[] replays;

    public ReplayResponse(Replay[] replays) {
        this.replays = replays;
    }

    public Replay[] getReplays() {
        return replays;
    }

    public void setReplays(Replay[] replays) {
        this.replays = replays;
    }
}
