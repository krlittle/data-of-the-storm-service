package com.dataofthestorm.service.models.response.replayresponse.replay;

import com.dataofthestorm.service.models.response.replayresponse.replay.player.Score;
import com.dataofthestorm.service.models.response.replayresponse.replay.player.Talent;

public class Player {

    private String hero;
    private int hero_level;
    private int team;
    private boolean winner;
    private int blizz_id;
    private int party;
    private boolean silenced;
    private String battletag;
    private Talent talents;
    private Score score;

    public String getHero() {
        return hero;
    }

    public void setHero(String hero) {
        this.hero = hero;
    }

    public int getHero_level() {
        return hero_level;
    }

    public void setHero_level(int hero_level) {
        this.hero_level = hero_level;
    }

    public int getTeam() {
        return team;
    }

    public void setTeam(int team) {
        this.team = team;
    }

    public boolean isWinner() {
        return winner;
    }

    public void setWinner(boolean winner) {
        this.winner = winner;
    }

    public int getBlizz_id() {
        return blizz_id;
    }

    public void setBlizz_id(int blizz_id) {
        this.blizz_id = blizz_id;
    }

    public int getParty() {
        return party;
    }

    public void setParty(int party) {
        this.party = party;
    }

    public boolean isSilenced() {
        return silenced;
    }

    public void setSilenced(boolean silenced) {
        this.silenced = silenced;
    }

    public String getBattletag() {
        return battletag;
    }

    public void setBattletag(String battletag) {
        this.battletag = battletag;
    }

    public Talent getTalents() {
        return talents;
    }

    public void setTalents(Talent talents) {
        this.talents = talents;
    }

    public Score getScore() {
        return score;
    }

    public void setScore(Score score) {
        this.score = score;
    }
}
