package com.dataofthestorm.service.models.response.replayresponse;

import com.dataofthestorm.service.models.response.replayresponse.replay.Player;

import java.util.ArrayList;

public class Replay {

    private int id;
    private String filename;
    private int size;
    private String game_type;
    private String game_date;
    private String game_map;
    private int game_length;
    private String game_version;
    private String fingerprint;
    private int region;
    private boolean processed;
    private String url;
    private String created_at;
    private String updated_at;
    private ArrayList<String[]> bans;
    private Player[] players;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getGame_type() {
        return game_type;
    }

    public void setGame_type(String game_type) {
        this.game_type = game_type;
    }

    public String getGame_date() {
        return game_date;
    }

    public void setGame_date(String game_date) {
        this.game_date = game_date;
    }

    public String getGame_map() {
        return game_map;
    }

    public void setGame_map(String game_map) {
        this.game_map = game_map;
    }

    public int getGame_length() {
        return game_length;
    }

    public void setGame_length(int game_length) {
        this.game_length = game_length;
    }

    public String getGame_version() {
        return game_version;
    }

    public void setGame_version(String game_version) {
        this.game_version = game_version;
    }

    public String getFingerprint() {
        return fingerprint;
    }

    public void setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
    }

    public int getRegion() {
        return region;
    }

    public void setRegion(int region) {
        this.region = region;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public ArrayList<String[]> getBans() {
        return bans;
    }

    public void setBans(ArrayList<String[]> bans) {
        this.bans = bans;
    }

    public Player[] getPlayers() {
        return players;
    }

    public void setPlayers(Player[] players) {
        this.players = players;
    }
}
