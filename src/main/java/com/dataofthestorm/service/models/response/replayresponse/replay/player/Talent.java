package com.dataofthestorm.service.models.response.replayresponse.replay.player;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Talent {

    @JsonProperty("1")
    private String level_1_talent;

    @JsonProperty("4")
    private String level_4_talent;

    @JsonProperty("7")
    private String level_7_talent;

    @JsonProperty("10")
    private String level_10_talent;

    @JsonProperty("13")
    private String level_13_talent;

    @JsonProperty("16")
    private String level_16_talent;

    @JsonProperty("20")
    private String level_20_talent;

    public String getLevel_1_talent() {
        return level_1_talent;
    }

    public void setLevel_1_talent(String level_1_talent) {
        this.level_1_talent = level_1_talent;
    }

    public String getLevel_4_talent() {
        return level_4_talent;
    }

    public void setLevel_4_talent(String level_4_talent) {
        this.level_4_talent = level_4_talent;
    }

    public String getLevel_7_talent() {
        return level_7_talent;
    }

    public void setLevel_7_talent(String level_7_talent) {
        this.level_7_talent = level_7_talent;
    }

    public String getLevel_10_talent() {
        return level_10_talent;
    }

    public void setLevel_10_talent(String level_10_talent) {
        this.level_10_talent = level_10_talent;
    }

    public String getLevel_13_talent() {
        return level_13_talent;
    }

    public void setLevel_13_talent(String level_13_talent) {
        this.level_13_talent = level_13_talent;
    }

    public String getLevel_16_talent() {
        return level_16_talent;
    }

    public void setLevel_16_talent(String level_16_talent) {
        this.level_16_talent = level_16_talent;
    }

    public String getLevel_20_talent() {
        return level_20_talent;
    }

    public void setLevel_20_talent(String level_20_talent) {
        this.level_20_talent = level_20_talent;
    }
}
