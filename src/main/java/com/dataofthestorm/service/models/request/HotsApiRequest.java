package com.dataofthestorm.service.models.request;

public class HotsApiRequest {

    private int page; /* Page to display, default to 1 */
    private String startDate; /* Minimum replayresponse date */
    private String endDate; /* Maximum replayresponse date */
    private String gameMap; /* Game map */
    private String gameType; /* Game type without spaces */
    private String player; /* Battle tag of a player */
    private boolean withPlayers; /* Whether to include player and ban data in response */

    public HotsApiRequest(String player) {
        this.page = 1;
        this.startDate = "2017-08-30";
        this.endDate = "2017-08-31";
        this.gameMap = null;
        this.gameType = "QuickMatch";
        this.player = player;
        this.withPlayers = true;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getGameMap() {
        return gameMap;
    }

    public void setGameMap(String gameMap) {
        this.gameMap = gameMap;
    }

    public String getGameType() {
        return gameType;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public boolean isWithPlayers() {
        return withPlayers;
    }

    public void setWithPlayers(boolean withPlayers) {
        this.withPlayers = withPlayers;
    }
}
