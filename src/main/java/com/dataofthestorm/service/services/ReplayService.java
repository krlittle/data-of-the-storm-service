package com.dataofthestorm.service.services;

import com.dataofthestorm.service.models.response.ReplayResponse;
import com.dataofthestorm.service.models.response.replayresponse.Replay;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ReplayService {

    @Value("${hotsapi.replays.url}")
    private String hotsApiReplaysUrl;

    @Autowired
    private RestTemplate restTemplate;

    public ReplayResponse getReplaysByPlayer() {
        return new ReplayResponse(restTemplate.getForEntity(hotsApiReplaysUrl, Replay[].class).getBody());
    }
}
